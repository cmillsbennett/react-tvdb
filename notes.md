#EPISODE DETAIL
This...
http://api.tvmaze.com/shows/210/episodebynumber?season=1&number=3
and this...
http://api.tvmaze.com/episodes/13859
are identical.


#DESIRED RESPONSE HIERARCHY
{
    seriesName, // the actual name of the series ('Doctor Who'), type: string
    seriesId, // the tvmaze id of the series ('210'), type: string
    seasonNum ,// the actual season number ('1'), type: string
    seasonId, // the tvmaze id of the season ('###'), type: string
    episodeDetail: { // created from model, type: object
        episodeTitle, // the actual title of the episode ('The Unquiet Dead'), type: string
        episodeSummary, // the tvmaze summary of the episode ('Charles Dickens and the Doctor team up as corpses stalk the streets of Victorian Cardiff.'), type: string
        episodeNum, // the actual number of the episode ('3'), type: string
        episodeId, // the tvmaze id of the episode ('13859'), type: string
    },
}

#TEMPLATE STRUCTURE
ShowsPage // page containing the ShowsList
ShowsList // list of all available shows
SeriesPage // page containing the series title and it's SeasonsList
SeasonsList  // list of all available seasons for a single series
SeasonPage // page containing the EpisodeList for the selected season
EpisodeList // list of all available episodes for a single season of a single series
EpisodePage // page containing all the individual episode details (title, summary, runtime, etc)