import get from 'lodash/get';

// export default class EpisodeModel {
//     // constructor(apiData) {
//     //     this.name = apiData.name;
//     //     this.season = apiData.season;
//     //     this.summary = apiData.summary;
//     //     this.thumbnail = get(apiData, 'image.medium', 'http://placecorgi.com/300/300 ');
//     // }
//     fromApiData(apiData) {
//         this.name = apiData.name;
//         this.season = apiData.season;
//         this.summary = apiData.summary;
//         this.airDate = apiData.airDate;
//         this.thumbnail = get(
//             apiData,
//             'image.medium',
//             'http://placecorgi.com/300/300 '
//         );
//     }
// }

export function buildEpisodeModel(apiData) {
    return {
        episodeId: apiData.id,
        episodeNum: apiData.number,
        episodeTitle: apiData.name,
        seasonNum: apiData.season,
        summary: apiData.summary.replace(/<[^>]*>?/gm, ''),
        thumbnail: get(
            apiData,
            'image.medium',
            'http://placecorgi.com/300/300 '
        ),
    };
}

export function buildSeasonModel(apiData) {
    // return {
    //     name: apiData.name,
    //     season: apiData.season,
    //     summary: apiData.summary,
    //     thumbnail: get(apiData, 'image.medium', 'http://placecorgi.com/300/300 ')
    // };
}
