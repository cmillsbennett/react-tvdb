import React, { Component } from 'react';
import axios from 'axios';
import ShowsList from './ShowsList';

class ShowsPage extends Component {
    state = {
        shows: null,
    };

    componentDidMount() {
        this.getAllShows();
    }

    async getAllShows() {
        // NOTE: Pass what you need as props down to child components
        const response = await axios.get('http://api.tvmaze.com/shows?page=0');
        const shows = response.data;
        this.setState({ shows });
    }

    render() {
        return (
            <div>
                <h1>Shows Page</h1>
                {this.state.shows ? (
                    <ShowsList shows={this.state.shows} />
                ) : (
                    'Loading'
                )}
            </div>
        );
    }
}

export default ShowsPage;
