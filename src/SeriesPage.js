import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import SeasonsList from './SeasonsList';

class ShowPage extends Component {
    state = {
        show: null,
        seasons: [],
    };

    static propTypes = {
        showId: PropTypes.string,
    };

    componentDidMount() {
        this.getSingleShow();
        this.getShowSeasons();
    }

    async getSingleShow() {
        const response = await axios.get(
            `http://api.tvmaze.com/shows/${this.props.showId}`
        );

        const show = response.data;
        this.setState({ show });
    }

    async getShowSeasons() {
        const response = await axios.get(
            `http://api.tvmaze.com/shows/${this.props.showId}/seasons`
        );
        const seasons = response.data;
        this.setState({ seasons });
    }

    render() {
        return (
            <div>
                <div>
                    {/* TODO: Convert to use state chart */}
                    <h1>
                        {this.state.show ? this.state.show.name : 'loading...'}{' '}
                        Show Detail Page
                    </h1>
                    <div>
                        <SeasonsList
                            show={this.state.show}
                            showId={this.props.showId}
                            seasons={this.state.seasons}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default ShowPage;
