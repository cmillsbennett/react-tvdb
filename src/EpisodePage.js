import React, { Component, Fragment } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { buildEpisodeModel } from './EpisodeModel';

class EpisodePage extends Component {
    state = {
        episode: [],
    };

    static propTypes = {
        showId: PropTypes.string,
        seasonId: PropTypes.string,
        seasonNumber: PropTypes.string,
        episodeId: PropTypes.string,
        episodeNumber: PropTypes.string,
    };

    componentDidMount() {
        this.getSingleEpisode();
    }

    async getSingleEpisode() {
        const response = await axios.get(
            `http://api.tvmaze.com/shows/${
                this.props.showId
            }/episodebynumber?season=${this.props.seasonId}&number=${
                this.props.episodeId
            }`
        );

        const episodeDetail = response.data;
        const episodeModel = buildEpisodeModel(episodeDetail);
        this.setState({ episode: episodeModel });
    }

    render() {
        return (
            <div>
                SINGLE SHOW EPISODE
                {this.state.episode ? (
                    <Fragment>
                        <h2>{this.state.episode.episodeTitle}</h2>
                        <p>{this.state.episode.summary}</p>
                    </Fragment>
                ) : (
                    'Loading'
                )}
            </div>
        );
    }
}

export default EpisodePage;
