import React, { Component } from 'react';
import { Link } from '@reach/router';
import PropTypes from 'prop-types';

class EpisodeList extends Component {
    state = {};

    static propTypes = {
        episodes: PropTypes.array,
        seasonId: PropTypes.string,
        showId: PropTypes.string,
        show: PropTypes.object,
    };

    render() {
        return (
            <div>
                {this.props.episodes.map(episode => (
                    // /show/210/season/1/episode/1
                    <Link
                        to={`/show/${this.props.showId}/season/${
                            episode.season
                        }/episode/${episode.number}`}
                        key={episode.id}
                    >
                        <h2>Season: {episode.season}</h2>
                        <h3>
                            {episode.number === null
                                ? `Episode Number: Special`
                                : `Episode Number: ${episode.number}`}
                        </h3>
                        <h3>Episode Name: {episode.name}</h3>
                    </Link>
                ))}
            </div>
        );
    }
}

export default EpisodeList;
