import React, { Component } from 'react';
import { Link } from '@reach/router';
import PropTypes from 'prop-types';

class SeasonsList extends Component {
    state = {
        show: this.props.show,
    };

    static propTypes = {
        show: PropTypes.array,
        showId: PropTypes.string,
        seasons: PropTypes.array,
    };

    render() {
        return (
            <div>
                <div>
                    {this.props.seasons.map(season => (
                        <Link to={`season/${season.id}`} key={season.id}>
                            <h2>Season {season.number}</h2>
                        </Link>
                    ))}
                </div>
            </div>
        );
    }
}

export default SeasonsList;
