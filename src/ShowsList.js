import React, { Component } from 'react';
import { Link } from '@reach/router';
import PropTypes from 'prop-types';

class ShowsList extends Component {
    static propTypes = {
        shows: PropTypes.array,
    };

    render() {
        return (
            <div>
                {this.props.shows.map(show => (
                    <Link key={show.id} to={`/show/${show.id}`}>
                        {show.name}
                    </Link>
                ))}
            </div>
        );
    }
}

export default ShowsList;
