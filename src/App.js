import React, { Component } from 'react';
import { Router } from '@reach/router';

import ShowsPage from './ShowsPage';
import SeriesPage from './SeriesPage';
import SeasonPage from './SeasonPage';
import EpisodePage from './EpisodePage';

import './App.css';

class App extends Component {
    render() {
        // Consider this the top level, wrapping component for the application
        return (
            <div className="App">
                <Router>
                    <ShowsPage path="/" />
                    <SeriesPage path="/show/:showId" />
                    <SeasonPage path="/show/:showId/season/:seasonId" />
                    <EpisodePage path="/show/:showId/season/:seasonId/episode/:episodeId" />
                </Router>
            </div>
        );
    }
}

export default App;
