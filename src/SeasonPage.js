import React, { Component } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import EpisodeList from './EpisodeList';

class SeasonPage extends Component {
    state = {
        seasonEpisodes: [],
    };

    static propTypes = {
        seasonId: PropTypes.string,
        showId: PropTypes.string,
        show: PropTypes.object,
    };

    componentDidMount() {
        this.getSeasonEpisodes();
    }

    async getSeasonEpisodes() {
        const response = await axios.get(
            // the season ID can be found in /shows/ID/seasons
            `http://api.tvmaze.com/seasons/${this.props.seasonId}/episodes`
        );
        const seasonEpisodes = response.data;
        this.setState({ seasonEpisodes });

        // get episdes for get season index - 1's real season id
        // extra credit - pass show through location.state, check for it and if youve got it, use that to get the real season id
    }

    render() {
        return (
            <div>
                {this.state.seasonEpisodes ? (
                    <EpisodeList
                        episodes={this.state.seasonEpisodes}
                        {...this.props}
                    />
                ) : (
                    'Loading'
                )}
            </div>
        );
    }
}

export default SeasonPage;
